﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class CharacterScript : MonoBehaviour 
{
	public int health = 100;
	public int damage = 10;

	public Animator anim;
	public Button button;

	public GameObject enemy;
	public Slider healthSlider;

	private CharacterScript enemyScript;

	private bool isDead = false;




	void Start()
	{
		enemyScript = enemy.GetComponent<CharacterScript> ();
		button.onClick.AddListener(Attack);
		healthSlider.value = health;

	}

	void Update()
	{
		
	}

	public void Hurt(int damage)
	{
		health -= damage;
		healthSlider.value = health;
		if (health <= 0) 
		{
			isDead = true;
			anim.SetBool ("isDead", true);
		}
	}

	void Attack()
	{
		StartCoroutine(AttackPerTime());

	}

	IEnumerator AttackPerTime()
	{
		anim.SetBool ("isPunching", true);
		if (Vector3.Distance (transform.position, enemy.transform.position) < 8f && !isDead )
			enemyScript.Hurt (damage);
		yield return new WaitForSeconds(2f);
		anim.SetBool ("isPunching", false);
	}
}
